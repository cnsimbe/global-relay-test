import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs"
import { tap, map } from "rxjs/operators"
import { HttpClient, HttpParams } from "@angular/common/http"
@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  constructor(private http:HttpClient) { }


  getTweets(q:string,limit:number):Observable<any[]> {

  	let params = new HttpParams().set('q',q).set('count',String(limit))

  	return this.http.jsonp<any>(`http://tweetsaver.herokuapp.com?${params.toString()}`,'callback')
  	.pipe(map(m=>m.tweets))
  }

}
