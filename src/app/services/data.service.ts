import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs"
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  private savedTweetKey = 'savedTweets'

  saveTweets(tweets):Observable<any> {
  	localStorage.setItem(this.savedTweetKey, JSON.stringify(tweets))
  	return of(true);
  }

  clearSavedTweets():Observable<any> {
  	localStorage.removeItem(this.savedTweetKey)
  	return of(true)
  }

  getSavedTweets():Observable<any[]> {
  	let tweets = localStorage.getItem(this.savedTweetKey)
  	return of(tweets ?  JSON.parse(tweets) : [])
  }

}
