import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';


import { TweetItemComponent } from '../components/tweet-item/tweet-item.component';
import { SearchTweetsComponent } from '../components/search-tweets/search-tweets.component';
import { SavedTweetsComponent } from '../components/saved-tweets/saved-tweets.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, TweetItemComponent, SearchTweetsComponent, SavedTweetsComponent]
})
export class HomePageModule {}
