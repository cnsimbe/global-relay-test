import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../../services/rest-api.service"
import { Observable, of } from "rxjs"
import { finalize } from "rxjs/operators"
import { LoadingController } from "@ionic/angular"


@Component({
  selector: 'app-search-tweets',
  templateUrl: './search-tweets.component.html',
  styleUrls: ['./search-tweets.component.scss']
})
export class SearchTweetsComponent implements OnInit {


  limit = 10;
  resultTweets:any[];

  constructor(private restApi:RestApiService, private loadCtrl:LoadingController) { }

  ngOnInit() {
  }

  onSubmitSearch(q:string) {
  	if(!q || q.length==0)
  		this.resultTweets =  []

  	this.loadCtrl.create()
  	.then(loader=>{
  		loader.present()
  		this.restApi.getTweets(q,this.limit)
  		.pipe(finalize(()=>loader.dismiss()))
  		.subscribe(tweets=>this.resultTweets=tweets)
  	})
  	
  }


  dragstart($event:DragEvent, tweet) {
  	$event.dataTransfer.setData("text",JSON.stringify(tweet))
  }

}
