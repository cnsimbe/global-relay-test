import { Component, OnInit } from '@angular/core';
import { DataService } from "../../services/data.service"
@Component({
  selector: 'app-saved-tweets',
  templateUrl: './saved-tweets.component.html',
  styleUrls: ['./saved-tweets.component.scss']
})
export class SavedTweetsComponent implements OnInit {

  constructor(private dataService:DataService) { }

  savedTweets:Array<any> = [];
  ngOnInit() {
  	this.dataService.getSavedTweets().subscribe(tweets=>this.savedTweets=tweets)
  }


  onTweetDrop($event:DragEvent) {
  	$event.preventDefault()
  	let tweet = JSON.parse($event.dataTransfer.getData("text"))

  	if(tweet && !this.savedTweets.find(st=>st.id==tweet.id))
  	{
  		this.savedTweets.unshift(tweet)
  		this.dataService.saveTweets(this.savedTweets).subscribe()
  	}
  }

  removeTweet(tweet) {
  	let index = this.savedTweets.indexOf(tweet)
  	if(index>=0)
  	{
  		this.savedTweets.splice(index,1)
  		this.dataService.saveTweets(this.savedTweets).subscribe()
  	}


  }

}
